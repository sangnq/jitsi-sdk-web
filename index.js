import VideoCallAPI from './VideoCallAPI';

const registerDevice = (deviceToken, uuidCustomer, topic, config) => {
    VideoCallAPI.registerDevice(deviceToken, uuidCustomer, topic, config);
};

const createUUID = () => {
    return VideoCallAPI.createUUID();
};

const createCall = async (caller, receiver, uuidCustomer, config) => {
    // const data = await VideoCallAPI.getDeviceIdByCustomerId(
    //     uuidCustomer,
    //     config
    // );
    // if (data.message === 'IDG-00000000' && data.object.DeviceIdSet) {
    //     const deviceIds = data.object.DeviceIdSet.split(';');
    //     return VideoCallAPI.createCall(
    //         caller,
    //         receiver,
    //         uuidCustomer,
    //         deviceIds,
    //         config
    //     );
    // }
    // return data;
    return VideoCallAPI.createCall(
        caller,
        receiver,
        uuidCustomer,
        config
    );
};

const acceptCall = (data, config) => {
    return VideoCallAPI.acceptCall(data, config);
};

const rejectCall = (data, config) => {
    return VideoCallAPI.rejectCall(data, config);
};

const endCall = (config) => {
    return VideoCallAPI.endCall(config);
};

const removeDevice = (config) => {
    return VideoCallAPI.removeDevice(config);
};

const VideoCallWeb = {
    registerDevice,
    createCall,
    createUUID,
    acceptCall,
    rejectCall,
    endCall,
    removeDevice,
};

export default VideoCallWeb;
