import Constant from './Constant';
import API from './API';
import ultils from './ultils';

function uuidv4() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (c) =>
        (
            c ^
            (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
        ).toString(16)
    );
}

function createUUID() {
    if (!ultils.getItem(Constant.UUID)) {
        ultils.setItem(Constant.UUID, uuidv4());
    }
    return uuidv4();
}

async function registerDevice(deviceToken, uuidCustomer, topic, config) {
    const body = {
        deviceId: ultils.getItem(Constant.UUID),
        deviceToken: deviceToken,
        deviceTypeId: 'WEB',
        idgTokenId: config.token_id,
        lastDate: new Date(),
        persionIdApp: uuidCustomer,
        topicUsing: topic,
    };
    return await API.postData(
        Constant.base_url + 'register-device',
        body,
        config
    );
}

async function getDeviceIdByCustomerId(uuidCustomer, config) {
    return await API.getData(
        Constant.base_url + 'get-one/' + config.token_id + '/' + uuidCustomer,
        '',
        config
    );
}

async function createCall(
    callerName = '',
    recevicerName = '',
    uuidCustomer,
    config
) {
    const data = await getDeviceIdByCustomerId(uuidCustomer, config);
    if (data.message === 'IDG-00000000' && data.object.DeviceIdSet) {
        const deviceIds = data.object.DeviceIdSet.split(';');
        const param = {
            callerName: callerName,
            deviceIdSrc: ultils.getItem(Constant.UUID),
            deviceIds: deviceIds,
            idgTokenId: config.token_id,
            recevicerName: recevicerName,
        };
        const res = await API.postData(
            Constant.base_url + 'create-call',
            param,
            config
        );
        ultils.setItem(Constant.ROOM_INFO, {
            roomId: res.object.roomId,
            token: res.object.token,
            domain: res.object.domain,
        });
        return res;
    }
    return data;
}

async function acceptCall(data, config) {
    const deviceIds = data.deviceIds.split(';');
    const param = {
        deviceId: ultils.getItem(Constant.UUID),
        deviceIds: deviceIds,
        idgTokenId: config.token_id,
        roomId: data.roomId,
    };
    ultils.setItem(Constant.ROOM_INFO, {
        roomId: data.roomId,
        token: data.token,
        domain: data.domain,
    });
    return await API.postData(Constant.base_url + 'accept-call', param, config);
}

async function rejectCall(data, config) {
    const deviceIds = data.deviceIds.split(';');
    const param = {
        deviceId: ultils.getItem(Constant.UUID),
        deviceIds: deviceIds,
        idgTokenId: config.token_id,
        roomId: data.roomId,
    };
    return await API.postData(Constant.base_url + 'reject-call', param, config);
}

async function endCall(config) {
    const roomInfo = JSON.parse(ultils.getItem(Constant.ROOM_INFO));
    const param = {
        deviceId: ultils.getItem(Constant.UUID),
        idgTokenId: config.token_id,
        roomId: roomInfo.roomId,
    };
    return await API.postData(Constant.base_url + 'end-call', param, config);
}

async function removeDevice(config) {
    const param = {
        deviceId: ultils.getItem(Constant.UUID),
        idgTokenId: config.token_id,
    };
    return await API.deleteData(
        Constant.base_url + 'remove-device',
        param,
        config
    );
}

const VideoCallAPI = {
    createUUID,
    registerDevice,
    getDeviceIdByCustomerId,
    createCall,
    acceptCall,
    rejectCall,
    endCall,
    removeDevice,
};

export default VideoCallAPI;
