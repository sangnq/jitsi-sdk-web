import Constant from './Constant';

const headerWithoutAuthor = {
    'Content-Type': 'application/json',
};

const headerWithAuthor = {
    'Content-Type': 'application/json',
    'Token-id': Constant.TOKEN_ID,
    'Token-key': Constant.TOKEN_KEY,
    'mac-address': 'WEB-001',
    Authorization: Constant.TOKEN,
};

async function postData(url = '', data = {}, config) {
    try {
        const response = await fetch(url, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            // withCredentials: true,
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
                'Token-id': config.token_id,
                'Token-key': config.token_key,
                'mac-address': 'WEB-001',
                Authorization: config.token,
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(data), // body data type must match "Content-Type" header
        });
        return response.json();
    } catch (e) {
        console.log(e);
        return e;
    }
}

async function getData(url = '', data = '', config) {
    const response = await fetch(url, {
        method: 'GET',
        mode: 'cors',
        withCredentials: true,
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json',
            'Token-id': config.token_id,
            'Token-key': config.token_key,
            'mac-address': 'WEB-001',
            Authorization: config.token,
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
    });
    return response.json();
}

async function putData(url = '', data = {}) {
    const response = await fetch(url, {
        method: 'PUT',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json',
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body: JSON.stringify(data),
    });
    return response;
}

async function deleteData(url = '', data = {}, config) {
    const response = await fetch(url, {
        method: 'DELETE',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json',
            'Token-id': config.token_id,
            'Token-key': config.token_key,
            'mac-address': 'WEB-001',
            Authorization: config.token,
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body: JSON.stringify(data),
    });
    return response.json();
}

const API = {
    getData,
    postData,
    putData,
    deleteData,
};
export default API;
