function setItem(key, value) {
    try {
        localStorage.setItem(key, JSON.stringify(value));
        return true;
    } catch (e) {
        console.log(e);
    }
}

function getItem(key) {
    try {
        return localStorage.getItem(key);
    } catch (e) {
        console.log(e);
    }
}

const ultils = {
    getItem,
    setItem,
};
export default ultils;
