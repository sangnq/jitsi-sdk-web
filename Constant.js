const base_url = 'https://explorer.idg.vnpt.vn/router-service/api/';
// const TOKEN =
//     'bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsicmVzdHNlcnZpY2UiXSwidXNlcl9uYW1lIjoiZG9hbnBoYW50aGFuaDEzMDVAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOlsiVVNFUiJdLCJqdGkiOiI3OTEzNjRjZi01OTFjLTQ2YmYtOGQ0Ny0yOTYzOTdiYWQ1NTQiLCJjbGllbnRfaWQiOiJhZG1pbmFwcCIsInNjb3BlIjpbInJlYWQiXX0.6EwQhluXY9ak1SLEqAvLxnFtqzBUXl-Dz3AdgIBuwmZwvQwTijm3GNbXmDvv0dAilk1_BUzKJDGyb8uvjgEv63NbCt22m04ox2RfejfiOO6_I7GqjD8HaQKYV8NyinluxObECBV7pUHzSY9oo9XtER1p0zoG5p4CdB2W_3rd1i-uxkSiKrOK_ifx4UPHxMP-2owrtPb9mK3ds-mfvO7bE_AMSrpykA35yXafPwOYOcgbnUWUSmVlhasqSDyan4EQHJ66l477P2uQXpA8sEF_oV3vfJ9daJlOyVVmoal2KuoU1pUGlG0lhdJJGmjxkGyXnyaKUxz58PSHZ_mJzq9qTw';
// const TOKEN_ID = 'aaa17433-e3c2-064f-e053-604fc10aab28';
// const TOKEN_KEY =
//     'MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAMlGceImXOf3AUE800nc/NfpIBNHT4nR1FaRSFADcyyMyXI8P9N8QWTsQnS5QrOGr5Sp2tP22xOfk+dpyRhlNzsCAwEAAQ==';
const UUID = 'uuid';
const ROOM_INFO = 'roomInfo';

const constant = {
    base_url,
    UUID,
    ROOM_INFO,
};

export default constant;
